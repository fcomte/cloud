# Content of the presentation
Presentation of the Insee innovation platform for the Bercy dataday (2019, November 29). 

:arrow_forward: [Slides](http://fcomte.gitlab.io/cloud/#/)

For obtaining a printable PDF, open the following link in Chrome browser and print with PDFcreator.

:arrow_forward: [Printable version](http://fcomte.gitlab.io/cloud/index.html?print-pdf#/)
