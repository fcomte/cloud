<!-- .slide: class="slide" -->

<!-- .slide: class="slide" -->
#### Anachronisme de l'Insee au rendez vous du cloud computing

1. Quelques notions sur le cloud
2. Revue de quelques projets / initiatives paradoxaux

---

<!-- .slide: class="slide" -->
## Quelques caractéristiques fondamentales sur le cloud
AWS 2006

1. accessiblité : service en ligne la plupart du temps 
2. automatisation de la mise à disposition
3. extensibilité idéale
4. autonomisation de l'utilisateur
5. nomadisme (cloud hybride, multicloud, indépendance par rapport à l'hebergeur)

---

<!-- .slide: class="slide" -->
## des offres à plusieurs niveaux

![](https://www.redhat.com/cms/managed-files/styles/wysiwyg_float/s3/iaas_focus-paas-saas-diagram-1200x1046.png?itok=lfcSWQpW)

----

<!-- .slide: class="slide" -->
## quelques offres dans le paysage

- le datalab , une offre SAAS de datascience et du CAAS pour les utilisateurs avancées
- NUBO, le cloud de la DGFIP une offre IAAS

---

<!-- .slide: class="slide" -->
##  Quelques paradoxes

l'Insee a 10000 VMs
- le dpii est responsable de la production de toutes les applications
- le dpii n'a pas une connaissance parfaite de l'utilité des machines en exploitation
- les demandeurs d'environnements ne sont pas facturées

----

<!-- .slide: class="slide" -->
##  Le seminaire devops

1. direction bonne mais 
2. le datascientist aujourd'hui est un consommateur des APIs Cloud ( IA, dataviz )

=> aller plus loin, le sujet de l'organisation de la fluidité des déploiements devrait englober l'ensemble de l'Insee et non simplement la DSI

---

<!-- .slide: class="slide" -->
##  PEA

Projet d'environnement d'administration
Les administrateurs auront un poste dédié aux taches d'administrations


---- 

<!-- .slide: class="slide" -->
## EDIFICE

bah grand chose à dire : aucune reflexion organisationnelle 


